import { Component ,OnInit,Inject} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {  EmployeeService } from './services/employee.service';
import { Employee } from './shared/employee';
import { MatDialog, MatDialogRef } from "@angular/material/dialog";
import { AddemployeeComponent } from './addemployee/addemployee.component';

//import{MatTableDataSource} from '@angular/material';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-alpine.css'
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'projectmanagement';

  private gridApi;
  private gridColumnApi;

  public columnDefs;
  private defaultColDef;
  private defaultColGroupDef;
  private columnTypes;




/*    defaultColDef: {
      // set every column width
      width: 100,
      // make every column editable
     // editable: true,
      // make every column use 'text' filter by default
      filter: 'agTextColumnFilter'
  }
*/
  /*columnDefs = [
    {headerName: 'Employee Code', field: 'code', sortable: true,filter: true},
    {headerName: 'Firat Name', field: 'firstname' , sortable: true,filter: true},
    {headerName: 'Last Name', field: 'lastname', sortable: true,filter: true},
    {headerName: 'Billing Rate', field: 'billingrate', sortable: true,filter: true},
    {headerName: 'Function', field: 'function', sortable: true,filter: true,}
];*/
  

 employees:Employee[];
 //rowData:any
/*rowData = [
    {code: 'Toyota', firstname: 'Celica', lastname: 'Chaudhri' },
    { code: 'Ford', firstname: 'Mondeo', lastname:"Patil" },
    { code: 'Porsche', firstname: 'Boxter', lastname: 'Rane' }
];*/
constructor(private http: HttpClient,private employeeSevice:EmployeeService, 
  @Inject('BaseURL') private BaseURL,public dialog: MatDialog ) {
    this.columnDefs = [
      {
        headerName: 'Employee Code', field: 'code', sortable: true,filter: true
      },
      {
        headerName: 'First Name', field: 'firstname' , sortable: true,filter: true
      },
      {
        headerName: 'Last Name', field: 'lastname', sortable: true,filter: true
      },
      {
        headerName: 'Billing Rate', field: 'billingrate', sortable: true,filter: true
      },
      {
        headerName: 'Function', field: 'function', sortable: true,filter: true
      },
      
    ];
    this.defaultColDef = {
      width: 2000,
      //editable: true,
      //filter: 'agTextColumnFilter',
      //floatingFilter: true,
      resizable: true,
    };
   
      this.employeeSevice.getEmployee().subscribe(employees => this.employees = employees);
  

}
ngOnInit() {
   

}
addEmployeeForm() {
  this.dialog.open(AddemployeeComponent, {width: '700px', height: '700px'});
}
}
