export class newEmployee{
    code:number;
    firstname: string;
    middlename: string;
    lastname: string;
    gender:string;
    title:string;
    initial:string;
    billingrate: number;
    function1: string;
    phonenumber:number;
    workphonenumber:number;
  }