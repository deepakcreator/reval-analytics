import { Employee } from './employee';

export const EMPLOYEES: Employee[] = [
    {
        code: 10,
        firstname: 'Deepak',
        
        lastname: 'Chaudhari',
        
        function: 'abcd',
        billingrate: 10,
        // tslint:disable-next-line:max-line-length
        
    },
    {
        code: 9,
        firstname: 'Chetan',
        
        lastname: 'Chaudhari',
        
        function: 'efgh',
        billingrate: 10.12,
    },
    {
        code: 20,
        firstname: 'Rajesh',
        
        lastname: 'Chaudhari',
        
        function: 'ijkl',
        billingrate: 4.99, 
    },
    {
        code: 130,
        firstname: 'sangita',
        
        lastname: 'Chaudhari',
        
        function: 'mnop',
        billingrate: 5,
    }
];