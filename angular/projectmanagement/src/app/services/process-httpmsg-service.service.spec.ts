import { TestBed } from '@angular/core/testing';

import { ProcessHttpmsgServiceService } from './process-httpmsg-service.service';

describe('ProcessHttpmsgServiceService', () => {
  let service: ProcessHttpmsgServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ProcessHttpmsgServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
