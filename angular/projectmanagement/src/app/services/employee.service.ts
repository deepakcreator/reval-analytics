import { Injectable } from '@angular/core';
import { Employee } from '../shared/employee';
import { EMPLOYEES } from '../shared/employees';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { baseURL } from '../shared/baseurl';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  constructor(private http: HttpClient) { }
  /*getEmployee(): Employee[] {
    return EMPLOYEES;
  }*/
  getEmployee(): Observable<Employee[]> {
    return this.http.get<Employee[]>("http://localhost:3000/" + 'employees');
  }
}
