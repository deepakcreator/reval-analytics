
import { Injectable } from '@angular/core';
import{HttpClient, HttpHeaders} from '@angular/common/http';
import{newEmployee} from '../shared/newemployee';
import { from } from 'rxjs';
import {catchError} from 'rxjs/operators';
import{Observable} from 'rxjs';
import {baseURL} from '../shared/baseurl'
import {ProcessHttpmsgServiceService } from '../services/process-httpmsg-service.service'
import {delay} from 'rxjs/operators'
//import { RestangularModule, Restangular } from 'ngx-restangular';
@Injectable({
  providedIn: 'root'
})
export class NewemployeeserviceService {
  submitemployee(employee: newEmployee): Observable<newEmployee> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
    };
    return this.http.post<newEmployee>(baseURL+'employees1', employee, httpOptions)
      .pipe(catchError(this.processHTTPMsgService.handleError)).pipe(delay(2000));
  }
  constructor(private http:HttpClient, private processHTTPMsgService:ProcessHttpmsgServiceService ) { }
}
