import { TestBed } from '@angular/core/testing';

import { NewemployeeserviceService } from './newemployeeservice.service';

describe('NewemployeeserviceService', () => {
  let service: NewemployeeserviceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NewemployeeserviceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
