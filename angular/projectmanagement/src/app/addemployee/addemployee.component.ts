import { Component, OnInit ,ViewChild} from '@angular/core';
import {MatDialog, MatDialogRef} from "@angular/material/dialog";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { newEmployee } from '../shared/newemployee';
import { NewemployeeserviceService } from '../services/newemployeeservice.service';
@Component({
  selector: 'app-addemployee',
  templateUrl: './addemployee.component.html',
  styleUrls: ['./addemployee.component.scss']
})
export class AddemployeeComponent implements OnInit {
  @ViewChild('fform') newemployeeFormDirective;
  newemplyeeForm: FormGroup;
  newemployee:newEmployee;
  updated: newEmployee;
  constructor(private fb: FormBuilder,private bd:NewemployeeserviceService) {
    this.createForm();
  }
/*
  createForm() {
    this.newemplyeeForm = this.fb.group({
      firstname: '',
      middlename: '',
      lastname: '',
      gender:'',
      title:'',
      initial:'',
      billingrate:0,
      function1: '',
      phonenumber:0,
      workphonenumber:0      
    });
}*/
formErrors = {
   'code':'',
  'firstname': '',
  'middlename': '',
  'lastname': '',
  'gender': '',
  'title': '',
  'initial': '',
  'billingrate': '',
  'function1': '',
  'phonenumber': '',
  'workphonenumber': ''
};
validationMessages = {
  'code': {
    'required':      'code is required.',
    'pattern':       'Code must contain only numbers.'
  },
 'firstname': {
   'required':      'First Name is required.',
   'minlength':     'First Name must be at least 2 characters long.',
   'maxlength':     'FirstName cannot be more than 25 characters long.'
 },
 'middlename': {
  'required':      'Middle Name is required.',
  'minlength':     'Middle Name must be at least 2 characters long.',
  'maxlength':     'Middle Name cannot be more than 25 characters long.'
},
 'lastname': {
   'required':      'Last Name is required.',
   'minlength':     'Last Name must be at least 2 characters long.',
   'maxlength':     'Last Name cannot be more than 25 characters long.'
 },
 'gender': {
  'required':      'gender is required.',
  'minlength':     'gender must be at least 2 characters long.',
  'maxlength':     'gender cannot be more than 6 characters long.'
},
'title': {
  'required':      'Title is required.',
  'minlength':     'Title must be at least 2 characters long.',
  
},
'initial': {
  'required':      'Initial is required.',
  'minlength':     'Initial must be at least 2 characters long.',
  
},
'billingrate': {
  'required':      'Billing rate is required.',
  'pattern':       'Billing rate must contain only numbers.'
},
'function1': {
  'required':      'Initial is required.',
  'minlength':     'Initial must be at least 2 characters long.',
  
},
 'phonenumber': {
   'required':      'Phone number is required.',
   'pattern':       'Phone number must contain only numbers.',
   'minlength':     ' Phone number must be at least 10 digit.'
 },
 'workphonenumber': {
  'required':      'Workphone number is required.',
  'pattern':       'Workphone must contain only numbers.',
  'minlength':     ' Work Phone number must be at least 10 digit.'
}
};
createForm(){
  this.newemplyeeForm = this.fb.group({
    code:[0,[Validators.required,Validators.maxLength(6),Validators.minLength(1)] ],
    firstname: ['', [Validators.required,Validators.maxLength(25),Validators.minLength(2)] ],
    middlename:['', [Validators.required,Validators.maxLength(25),Validators.minLength(2)] ],
    lastname: ['', [Validators.required,Validators.maxLength(25),Validators.minLength(2)]],
    gender:['', [Validators.required,Validators.maxLength(6),Validators.minLength(2)] ],
    title:['', [Validators.required,Validators.minLength(2)]],
     initial:['', [Validators.required,Validators.minLength(2)] ],
     billingrate:['',[Validators.required,Validators.pattern]],
     function1:['', [Validators.required,Validators.minLength(2)] ],
     phonenumber: [0, [Validators.required,Validators.pattern,Validators.minLength(10)] ],
     workphonenumber: [0,[Validators.required,Validators.pattern,Validators.minLength(10)] ]
  })
  this.newemplyeeForm.valueChanges
  .subscribe(data => this.onValueChanged(data));
  this.onValueChanged();
}
 onValueChanged(data?: any) {
      if (!this.newemplyeeForm) { return; }
      const form = this.newemplyeeForm;
      for (const field in this.formErrors) {
        if (this.formErrors.hasOwnProperty(field)) {
          // clear previous error message (if any)
          this.formErrors[field] = '';
          const control = form.get(field);
          if (control && control.dirty && !control.valid) {
            const messages = this.validationMessages[field];
            for (const key in control.errors) {
              if (control.errors.hasOwnProperty(key)) {
                this.formErrors[field] += messages[key] + ' ';
              }
            }
          }
        }
      }
    }
/*
onSubmit() {
  this.newemployee = this.newemplyeeForm.value;
    console.log(this.newemployee);
    this.newemplyeeForm.reset({
    firstname: '',
      middlename: '',
      lastname: '',
      gender:'',
      title:'',
      initial:'',
      billingrate:0,
      function1: '',
      phonenumber:0,
      workphonenumber:0 
  });
  this.newemployeeFormDirective.resetForm();
}*/
/*
onSubmit() {
  
  setTimeout(() => {
    this.newemployee = this.newemplyeeForm.value;
    console.log(this.newemployee);
  
  this.bd.submitemployee(this.newemployee).subscribe( feed => {
    this.updated=feed;
    
    setTimeout(() => {
      this.updated=null;
    }, 5000);
  });
  this.newemplyeeForm.reset({
    code:0,
    firstname: '',
    middlename: '',
    lastname: '',
    gender:'',
    title:'',
    initial:'',
    billingrate:0,
    function1: '',
    phonenumber:0,
    workphonenumber:0 
  });
  }, 2000);
  this.newemployeeFormDirective.resetForm();
}*/
/*  onSubmit() {
    this.newemployee = this.newemplyeeForm.value;
    console.log(this.newemployee);
    this.newemplyeeForm.reset();
  }*/
  onSubmit() {
    this.updated = this.newemplyeeForm.value;
    
    this.newemployee = this.newemplyeeForm.value;
    this.bd.submitemployee(this.newemployee)
      .subscribe(employee=> 
        {  
          
           this.updated =employee; 
            console.log(this.updated); 
           //x setTimeout(() => this.feedback = null, 5000);
          
        }
      );
 


     

    //console.log(this.feedback);

    this.newemplyeeForm.reset({
      code:0,
    firstname: '',
    middlename: '',
    lastname: '',
    gender:'',
    title:'',
    initial:'',
    billingrate:0,
    function1: '',
    phonenumber:0,
    workphonenumber:0 

    });
    this.newemployeeFormDirective.resetForm();

  }

  ngOnInit(): void {
  }

}
